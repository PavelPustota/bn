# bn.ru

## Устроновка

1) Склонировать репозиторий
```
$ git clone https://PavelPustota@bitbucket.org/PavelPustota/bn.git
```

2) Установить зависимости
```
$ composer install
```

3) Создать директорию для кэша
```
$ mkdir data
$ chmod 777 ./data
```

## Конфигурация nginx

```
server {
    charset utf-8;
    client_max_body_size 128M;

    listen 80;

    server_name bn.dev;
    root        /path/to/projects/bn/public;
    index       index.php;

    access_log  /path/to/nginx/logs/bn.dev.access.log;
    error_log   /path/to/nginx/logs/bn.dev.error.log;

    location / {
        try_files $uri $uri/ /index.php?$args;
    }

    location ~ \.(js|css|png|jpg|gif|swf|ico|pdf|mov|fla|zip|rar)$ {
        try_files $uri =404;
    }

    location ~ \.php$ {
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root/$fastcgi_script_name;
        fastcgi_param   APPLICATION_ENV  dev;
        fastcgi_pass   127.0.0.1:9000;
        try_files $uri =404;
    }

    location ~ /\.(ht|svn|git) {
        deny all;
    }
}
```