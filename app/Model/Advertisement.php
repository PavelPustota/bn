<?php
namespace App\Model;

final class Advertisement extends BaseModel
{
    protected $room;
    protected $address;
    protected $floor;
    protected $type;
    protected $square;
    protected $wc;
    protected $price;
    protected $subject;
    protected $phone;
    protected $add;
}