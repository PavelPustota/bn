<?php
namespace App\Model;

use \Exception;
use App\Common\Curl;
use Sunra\PhpSimple\HtmlDomParser;

final class SubwayStation extends BaseModel
{
    protected $id;
    protected $name;

    public static function LoadSubwayStations()
    {
        $res = [];

        // TODO Пришло время для конфигурационного файла
        $dataDirPath = dirname(__FILE__) . '/../../data';
        $dataFilePath = dirname(__FILE__) . '/../../data/subway_station.json';

        if (false === file_exists($dataFilePath)) {
            // TODO Возможно стоит загрузку вынести в отдельный метод?!
            if (!is_writeable($dataDirPath)) {
                throw new Exception('The data directory is not writeable');
            }

            $curl = new Curl('http://www.bn.ru/');
            $html = $curl->exec();
            if($html){
                $html = HtmlDomParser::str_get_html($html);

                $metro = $html->find('div.metro', 0);
                $stations = $metro->find('a.select_check');

                $result = [];
                foreach ($stations as $station) {
                    $result[] = [
                        'id' => trim($station->value),
                        'name' => trim($station->plaintext),
                    ];
                }

                if (false === file_put_contents($dataFilePath, json_encode($result), LOCK_EX)){
                    throw new Exception('Could not save data file');
                };
            }
        }

        $json = file_get_contents($dataFilePath);
        $stations = json_decode($json, true);

        foreach ($stations as $station) {
            $res[] = new SubwayStation($station);
        }

        return $res;
    }
}