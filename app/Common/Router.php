<?php
namespace App\Common;

use \Exception;

class Router {
    private $registry;

    private $path;

    private $args = [];

    function __construct($registry) {
        $this->registry = $registry;
    }

    function setPath($path)
    {
        if (is_dir($path) == false) {
            throw new Exception('Invalid controller path: `' . $path . '`');
        }
        $this->path = $path;
    }

    function delegate()
    {
        $this->getController($file, $controller, $action, $args);

        if (is_readable($file) == false) {
            // TODO граммотно обработать и вывести 404 страницу
            die ('404 Not Found');
        }

        $class = 'App\Controller\\' . ucfirst(strtolower($controller)) . 'Controller';
        $controller = new $class($this->registry);

        $action = 'action' . ucfirst(strtolower($action));

        if (is_callable([$controller, $action]) == false) {
            // TODO граммотно обработать и вывести 404 страницу
            die ('404 Not Found');
        }

        $controller->$action();
    }

    private function getController(&$file, &$controller, &$action, &$args)
    {
        // TODO ЧПУ URL
        $route = (empty($_GET['route'])) ? 'index' : $_GET['route'];

        $parts = explode('/', $route);

        $controller = empty($parts[0]) ? 'index' : $parts[0];

        $action = empty($parts[1]) ? 'index' : $parts[1];

        $file = $this->path . '/' . ucfirst(strtolower($controller)) . 'Controller' . '.php';

        $args = [];
    }
}