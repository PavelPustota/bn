<?php
namespace App\Common;

use \Exception;

class Curl
{
    const DEFAULT_TIMEOUT = 10;
    const DEFAULT_USERAGENT = 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0)';

    public $curl;

    public function __construct($url = null)
    {
        if (!extension_loaded('curl')) {
            throw new Exception('cURL library is not loaded');
        }

        $this->curl = curl_init();
        curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->curl, CURLOPT_TIMEOUT, self::DEFAULT_TIMEOUT);
        curl_setopt($this->curl, CURLOPT_URL, $url);
        curl_setopt($this->curl, CURLOPT_USERAGENT, self::DEFAULT_USERAGENT);
    }

    public function exec()
    {
        return curl_exec($this->curl);
    }
}