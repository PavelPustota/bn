<?php
namespace App\Common;

class Template
{
    private $registry;

    private $vars = [];

    function __construct($registry)
    {
        $this->registry = $registry;
    }

    function set($varName, $value, $overwrite=false)
    {
        if (isset($this->vars[$varName]) == true AND $overwrite == false) {
            trigger_error(htmlentities('Не удалось установить Var `' . $varName . '`. Уже установлен, и замены не допускаются.'), E_USER_NOTICE);
            return false;
        }
        $this->vars[$varName] = $value;
        return true;
    }

    function remove($varName)
    {
        unset($this->vars[$varName]);
        return true;
    }

    function show($name)
    {
        $path = dirname(__FILE__) . '/../Views/' . $name . '.php';

        if (file_exists($path) == false) {
            trigger_error(htmlentities('Шаблон `' . $name . '` не определён.'), E_USER_NOTICE);
            return false;
        }

        foreach ($this->vars as $key => $value) {
            $$key = $value;
        }

        include ($path);
    }
}