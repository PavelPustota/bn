<div class="row">
    <div class="col-sm-12">
        <h1>Главная</h1>
        <form class="form-horizontal well" action="/" method="POST">
            <div class="form-group">
                <label for="roomsFrom" class="col-sm-2 control-label">Кол-во комнат</label>
                <div class="col-sm-5">
                    <input type="number" class="form-control" id="roomsFrom" name="roomsFrom" placeholder="От" value="<?php echo $params['roomsFrom']?>">
                </div>
                <div class="col-sm-5">
                    <input type="number" class="form-control" id="roomsTo" name="roomsTo" placeholder="До" value="<?php echo $params['roomsTo']?>">
                </div>
            </div>
            <div class="form-group">
                <label for="priceFrom" class="col-sm-2 control-label">Цена</label>
                <div class="col-sm-5">
                    <input type="number" class="form-control" id="priceFrom" name="priceFrom" placeholder="От" value="<?php echo $params['priceFrom']?>">
                </div>
                <div class="col-sm-5">
                    <input type="number" class="form-control" id="priceTo" name="priceTo" placeholder="До" value="<?php echo $params['priceTo']?>">
                </div>
            </div>
            <div class="form-group">
                <label for="stations" class="col-sm-2 control-label">Метро</label>
                <div class="col-sm-10">
                    <select multiple class="form-control" id="stations" name="stations[]">
                        <option value="-1">Не выбрано</option>
                        <?php
                        if (isset($stations)) {
                            foreach ($stations as $index => $station) {
                                $selected = (in_array($station->id, $params['stations'])) ? 'selected="selected"' : '';
                                echo '<option value="'.$station->id.'" '.$selected.'>'.$station->name.'</option>';
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-10 col-sm-offset-2">
                    <button type="submit" class="btn btn-default">Найти</button>
                </div>
            </div>
        </form>
    </div>
</div>
<?php if(!empty($items)):?>
<div class="row">
    <div class="col-xs-12">
        <table class="table">
            <thead>
                <tr>
                    <th>Издание</th>
                    <th>Кол-во комнат</th>
                    <th>Адрес</th>
                    <th>Метро</th>
                    <th>Этаж</th>
                    <th>Тип дома</th>
                    <th>Площадь (общая, жилая, кухня)</th>
                    <th>Телефон</th>
                    <th>Санузел</th>
                    <th>Субъект</th>
                    <th>Контакт</th>
                    <th>Доп. сведения</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    foreach ($items as $index => $item) {
                        echo '<tr>';
                        echo '<td>'.'</td>';
                        echo '<td>' . $item->room .'</td>';
                        echo '<td>' . $item->address .'</td>';
                        echo '<td>' .'</td>';
                        echo '<td>' . $item->floor .'</td>';
                        echo '<td>' . $item->type .'</td>';
                        echo '<td>' . $item->square .'</td>';
                        echo '<td>' . $item->price .'</td>';
                        echo '<td>' . $item->wc .'</td>';
                        echo '<td>' . $item->subject .'</td>';
                        echo '<td>' . $item->phone .'</td>';
                        echo '<td>' . $item->add .'</td>';
                        echo '</tr>';
                    }
                ?>
            </tbody>
        </table>
    </div>
</div>
<?php endif;?>