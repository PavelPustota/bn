<?php
namespace App\Controller;

abstract class BaseController
{
    protected $registry;

    function __construct($registry)
    {
        $this->registry = $registry;
    }

    abstract function actionIndex();
}