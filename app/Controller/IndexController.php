<?php
namespace App\Controller;

use Sunra\PhpSimple\HtmlDomParser;
use App\Model\Advertisement;
use App\Model\SubwayStation;
use App\Common\Curl;

final class IndexController extends BaseController
{
    function actionIndex()
    {
        try {
            $stations = SubwayStation::LoadSubwayStations();
        } catch (\Exception $e){
            $stations = [];
        }
        $items = [];

        $filters = [
            'roomsFrom' => FILTER_VALIDATE_INT,
            'roomsTo' => FILTER_VALIDATE_INT,
            'priceFrom' => FILTER_SANITIZE_ENCODED,
            'priceTo' => FILTER_SANITIZE_ENCODED,
            'stations' => [
                'filter' => FILTER_VALIDATE_INT,
                'flags'  => FILTER_REQUIRE_ARRAY,
            ],
        ];

        if(isset($_POST)) {
            $params = filter_input_array(INPUT_POST, $filters);
            $this->registry['view']->set('params', $params);
        }

        if(isset($params)) {
            $url = "http://www.bn.ru/zap_fl.phtml";
            $query = http_build_query([
                'kkv1' => $params['roomsFrom'],
                'kkv2' => $params['roomsTo'],
                'price1' => $params['priceFrom'],
                'price2' => $params['priceTo'],
                'metro' => $params['stations'],
            ]);

            $url .= '?' . $query;

            $curl = new Curl($url);
            $html = $curl->exec();

            // TODO phpquery плохо женится, не нашёл кошерный вариант на packagist
            if($html) {
                $html = HtmlDomParser::str_get_html($html);

                $table = $html->find('table.results tbody tr');

                // TODO Нельзя доверять входным данным
                foreach ($table as $row) {
                    $columns = $row->find('td');
                    if (count($columns) <= 3) {
                        continue;
                    } elseif (count($columns) == 10) {
                        $advertisementData = [
                            'room' => '',
                            'address' => trim(mb_convert_encoding($columns[1]->plaintext, 'utf-8', 'Windows-1251')),
                            'floor' => mb_convert_encoding($columns[3]->plaintext, 'utf-8', 'Windows-1251'),
                            'type' => mb_convert_encoding($columns[4]->plaintext, 'utf-8', 'Windows-1251'),
                            'square' => mb_convert_encoding($columns[5]->plaintext, 'utf-8', 'Windows-1251') . '/' . mb_convert_encoding($columns[6]->plaintext, 'utf-8', 'Windows-1251'),
                            'wc' => '',
                            'price' => mb_convert_encoding($columns[6]->plaintext, 'utf-8', 'Windows-1251'),
                            'subject' => mb_convert_encoding($columns[7]->plaintext, 'utf-8', 'Windows-1251'),
                            'phone' => mb_convert_encoding($columns[8]->plaintext, 'utf-8', 'Windows-1251'),
                            'add' => mb_convert_encoding($columns[9]->plaintext, 'utf-8', 'Windows-1251'),
                        ];
                    } else {
                        $advertisementData = [
                            'room' => mb_convert_encoding($columns[1]->plaintext, 'utf-8', 'Windows-1251'),
                            'address' => trim(mb_convert_encoding($columns[2]->plaintext, 'utf-8', 'Windows-1251')),
                            'floor' => mb_convert_encoding($columns[3]->plaintext, 'utf-8', 'Windows-1251'),
                            'type' => mb_convert_encoding($columns[4]->plaintext, 'utf-8', 'Windows-1251'),
                            'square' => mb_convert_encoding($columns[5]->plaintext, 'utf-8', 'Windows-1251') . '/' . mb_convert_encoding($columns[6]->plaintext, 'utf-8', 'Windows-1251') . '/' . mb_convert_encoding($columns[7]->plaintext, 'utf-8', 'Windows-1251'),
                            'wc' => mb_convert_encoding($columns[8]->plaintext, 'utf-8', 'Windows-1251'),
                            'price' => mb_convert_encoding($columns[9]->plaintext, 'utf-8', 'Windows-1251'),
                            'subject' => mb_convert_encoding($columns[11]->plaintext, 'utf-8', 'Windows-1251'),
                            'phone' => mb_convert_encoding($columns[12]->plaintext, 'utf-8', 'Windows-1251'),
                            'add' => mb_convert_encoding($columns[13]->plaintext, 'utf-8', 'Windows-1251'),
                        ];
                    }

                    $items[] = new Advertisement($advertisementData);
                }
            }
        }

        $this->registry['view']->set('page', 'index');
        $this->registry['view']->set('stations', $stations);
        $this->registry['view']->set('items', $items);
        $this->registry['view']->show('main');
    }

    function actionAjax()
    {
        if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || $_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest') {
            return;
        }
    }
}