<?php
namespace App;

use \Exception;

use App\Common\Registry;
use App\Common\Template;
use App\Common\Router;

class Core
{
    private static $_instance = null;

    private function __construct()
    {

    }

    protected function __clone()
    {

    }

    static public function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function run()
    {
        try {
            $registry = new Registry;

            $template = new Template($registry);
            $registry->set('view', $template);

            $router = new Router($registry);
            $registry->set('router', $router);

            $router->setPath(dirname(__FILE__) . '/Controller');
            $router->delegate();
        } catch (Exception $e) {
            // TODO Красиво вывести ошибку
            // TODO Только в development окружении выводить
            print_r($e);
        }
    }
}