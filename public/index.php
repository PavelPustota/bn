<?php
require_once dirname(__FILE__) . '/../vendor/autoload.php';

use \App\Core;

$app = Core::getInstance();

$app->run();